import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useReceiptStore } from './receipt'

export const usePaymentTypeStore = defineStore('paymentType', () => {

    const paymentType = ref()
    const receiptStore = useReceiptStore()

    function addPaymentType(type: string) {
        paymentType.value = type
        receiptStore.receipt.paymentType = type
        console.log('paymentType', receiptStore.receipt.paymentType)
    }

  return { paymentType, addPaymentType }
})
