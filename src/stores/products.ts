import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import { type Product } from '@/types/Product';

export const useProductsStore = defineStore('products', () => {
    const products1:Product[] = [
        { id: 1, name: 'ลาเต้ ร้อน', price: 35.00, type: 1 },
        { id: 2, name: 'โกโก้ ร้อน', price: 45.00, type: 1 },
        { id: 3, name: 'เอสเพรสโซ ร้อน', price: 55.00, type: 1 },
        { id: 4, name: 'ชาเขียว ร้อน', price: 35.00, type: 1 },
        { id: 5, name: 'มอคค่า ร้อน', price: 40.00, type: 1 },
        { id: 6, name: 'อเมริกาโน ร้อน', price: 50.00, type: 1 },
        { id: 7, name: 'มัคคิอาโต ร้อน', price: 45.00, type: 1 },
        { id: 8, name: 'ชามะนาว ร้อน', price: 30.00, type: 1 },
        { id: 9, name: 'คาปูชิโน ร้อน', price: 55.00, type: 1 }
    ]
    
    const products2:Product[] = [
        { id: 10, name: 'เค้กโรลสตรอเบอร์รี่', price: 60.00, type: 2 },
        { id: 11, name: 'เค้กโรลช็อคโกแลต', price: 55.00, type: 2 }
    ]
    
    const products3:Product[] = [
        { id: 12, name: 'ไข่กระทะ', price: 40.00, type: 3 },
        { id: 13, name: 'สปาเก็ตตี้คาโบนาร่า', price: 50.00, type: 3 },
        { id: 14, name: 'ข้าวไข่เจียวหมูสับ', price: 35.00, type: 3 }
    ]

  return { products1, products2, products3 }
})
