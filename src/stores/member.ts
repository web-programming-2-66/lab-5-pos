import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
    const members = ref<Member[]>([
        { id: 1, name: 'ซอง จินอู', tel: '0812345678' },
        { id: 2, name: 'โกโจ ซาโตรุ', tel: '0823456789' },
        { id: 3, name: 'อิตาโดริ ยูจิ', tel: '0845678910' }
    ])

    const currentMember = ref<Member | null>()
    const searchMember = function(tel: string): Member | null {
        const index = members.value.findIndex((item) => item.tel === tel)
        if (index<0) {
            currentMember.value = null
        }
        currentMember.value = members.value[index]
    }
    function clearMember() {
        currentMember.value = null
    }
    return { 
        members,
        currentMember,
        searchMember, clearMember
    }
})
