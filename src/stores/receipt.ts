import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'

import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Product } from '@/types/Product'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import { useMemberStore } from './member'

export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const memberStore = useMemberStore()
  const receiptDialog = ref(false)
  const receipt = ref<Receipt>({
      id: -1,
      createdDate: new Date(),
      total: 0.00,
      discount: 0.00,
      netAmount: 0.00,
      receivedAmount: 0.00,
      change: 0.00,
      paymentType: '',
      userId: authStore.currentUser.id,
      user: authStore.currentUser,
      memberId: -1
  })
  const receiptItems = ref<ReceiptItem[]>([])
  function addReceiptItem(product: Product) {

    const index = receiptItems.value.findIndex((item) => item.productId === product.id)
    if (index >= 0) {
        receiptItems.value[index].unit++
        calReceipt()
        return
    } else {
        const newReceipt:ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id,
        product: product
    }
    receiptItems.value.push(newReceipt)
    calReceipt()
    }
    
  }
  function removeReceiptItem(receiptItem: ReceiptItem) {
      const index = receiptItems.value.findIndex((item) => item === receiptItem)
      receiptItems.value.splice(index, 1)
      calReceipt()
  }
  function increaseItem(item: ReceiptItem) {
      item.unit++
      calReceipt()
  }
  function decreaseItem(item: ReceiptItem) {
      if(item.unit === 1){
          removeReceiptItem(item)
      } else {
          item.unit--
          calReceipt()
      }
  }
 
  function calReceipt() {
    let total = 0
    for (const item of receiptItems.value) {
      total += item.price * item.unit
    }
    receipt.value.total = total
    if(memberStore.currentMember) {
      receipt.value.netAmount = total * 0.95
      receipt.value.discount = receipt.value.total - receipt.value.netAmount
    } else {
      receipt.value.netAmount = receipt.value.total
    }
    
  }
  function confirmPayment() {
    receipt.value.memberId = memberStore.currentMember?.id ?? -1
    receipt.value.receiptItems = receiptItems.value
    showReceiptDialog()
  }
  function showReceiptDialog() {
    receiptDialog.value = true
  } 
  function clearReceipt() {
    receiptItems.value = []
    receipt.value = {
        id: -1,
        createdDate: new Date(),
        total: 0.00,
        discount: 0.00,
        netAmount: 0.00,
        receivedAmount: 0.00,
        change: 0.00,
        paymentType: '',
        userId: authStore.currentUser.id,
        user: authStore.currentUser,
        memberId: -1
    }
    memberStore.clearMember()
  }

  return { receipt, receiptItems, receiptDialog, 
    addReceiptItem, removeReceiptItem, increaseItem, 
    decreaseItem, calReceipt, confirmPayment,showReceiptDialog, clearReceipt }
})
