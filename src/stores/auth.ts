import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { User } from '@/types/User'

export const useAuthStore = defineStore('auth', () => {
    const currentUser = ref<User>({
        id: -1,
        email: 'pee123',
        password: '1234',
        fullName: 'พีสุุเกะ',
        gender: 'male',
        role: ['user']
    })

  return { currentUser }
})
