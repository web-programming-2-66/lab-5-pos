type Gender = 'male' | 'female' | 'other'
type Role = 'admin' | 'user'
type User = {
    id: number
    email: string
    password: string
    fullName: string
    gender: Gender // male, female, other
    role: Role[] // admin, user
}

export type { Gender, Role, User }