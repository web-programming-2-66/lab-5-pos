import type { Member } from './Member';
import type { ReceiptItem } from './ReceiptItem';
import type { User } from './User';

type Receipt = {
    id: number;
    createdDate: Date;
    total: number;
    discount: number;
    netAmount: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member?: Member;
    receiptItems?: ReceiptItem[];
}

export type { Receipt }